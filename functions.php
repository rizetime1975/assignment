<?php

add_action( 'wp_enqueue_scripts', 'assignment_theme_scripts' );

function assignment_theme_scripts() {

    $public_path = get_template_directory_uri() . '/public';
    $assets_path = get_template_directory_uri() . '/public/assets';

    wp_enqueue_style( 'index-style', $assets_path . '/css/index.min.css' );
    wp_enqueue_script( 'index-script',  $assets_path . '/js/index.js', array(), '1.0.0', true );

    wp_register_script('localize-script', $assets_path . '/js/localize-path.js');
    wp_enqueue_script('localize-script');
    wp_localize_script( 'localize-script', 'localizePath', array(
        'assets' => __( $assets_path ),
        'public' => __( $public_path ),
        'img' => __( $assets_path . '/img' ),
    ) );
}

register_nav_menus(array(
    'top'    => 'header menu',
    'bottom' => 'footer menu'
));

add_theme_support( 'title-tag' );

// Remove Site Name from Page Title
add_filter( 'document_title_parts', function( $title )
{
    if ( is_home() || is_front_page() )
        unset( $title['title'] ); /** Remove title name */

    if ( is_single() )
        unset( $title['site'] ); /** Remove site name */

    return $title;

}, 10, 1 );


add_filter('widget_text', 'do_shortcode');
add_shortcode('cta', 'cta_shortcode');

function cta_shortcode($atts){
    $link = $atts['link'];
    $title = $atts['title'];
    return "
        <div class='wp-block-cgb-cta-block-gutenberg-blocks'>
            <div class='cta-block'>
                <div class='cta-block__paragraph'>$title</div>
                <a href='$link' class='cta-block__link'>Visit Site</a>
            </div>
        </div>
";
}







