<?php
/* Template Name: Category */

the_post();

get_header();

?>

<?php get_template_part('public/tpl/banner'); ?>
<?php get_template_part('public/tpl/slider-top-services'); ?>

    <div id="content-block">
        <div class="container">
            <?php echo apply_filters('the_content', get_the_content()); ?>
        </div>
    </div><!-- #content-block -->

<?php
get_footer();