
export const usersCounterSaved = ($, selector='#users-counter', timer=5) => {
    const setUsers = () => {
        localStorage.setItem('users', `${parseInt(localStorage.getItem('users')) + 1}`);
        $(selector).text( localStorage.getItem('users') )
    }
    if (localStorage.getItem('users') === null) {
        localStorage.setItem('users', `105`);
        $(selector).text(
            localStorage.getItem('users')
        )
    } else {
        setUsers();
        setInterval(setUsers,timer * 1000);
    }
};

export const usersCounter = ($, selector= null, timer=3) => {
    if (selector !== null) {
        setInterval(()=>{
            let value = parseInt(selector.textContent);
            value++;
            selector.innerText = `${value}`;
        },timer * 1000)
    }
}