export const addCtaBg = ($, imgPath, selector= '.cta-block') => {
    $(selector).css({
        background: `#7e4ad0 url(${imgPath}/cta-bg.png) no-repeat center center / cover`
    })
}

//#7e4ad0