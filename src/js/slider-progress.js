export const sliderProgress = ($) => {

    let $slider = $('.slider-box-road');
    let $progressBar = $('.progress');
    let $progressBarLabel = $( '.slider__label' );

    $slider.on('beforeChange', function(event, slick, currentSlide, nextSlide) {
        let calc = ( (nextSlide) / (slick.slideCount-1) ) * 100;

        $progressBar
            .css('background-size', calc + '% 100%')
            .attr('aria-valuenow', calc );

        $progressBarLabel.text( calc + '% completed' );
    });
}