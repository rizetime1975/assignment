export const simpleLazyLoad = ($) => {
    $('[data-lazy]')
        .attr('src', $('[data-lazy]').attr('data-lazy'))
        .attr('data-lazy', 'confirmed')

    setTimeout(()=>{
        $('[data-lazy]').removeClass('blur');
    }, 400);

}