<section id="banner" class="banner">
    <div class="container">
        <div class="banner-box">
            <div class="info">
                <div class="info__sup-text">by Lorem Ipsum</div>
                <h1 class="info__title">
                    <?php echo wp_get_document_title(); ?>
                </h1>
                <div class="info__sub-text">Lorem Ipsum is simply dummy text</div>
                <div class="main-counter">
                    <div class="main-counter__item">65 mln</div>
                    <div class="main-counter__item">12000</div>
                    <div class="main-counter__item"><span id="users-counter"> <!-- counter --> </span></div>
                </div>
                <p class="info__description">
                    Lorem Ipsum is simply dummy text of the printing and typesetting
                    industry. Lorem Ipsum has been the industry's standard dummy text
                    ever since the 1500s, when an unknown printer took a galley of type
                    and scrambled it to make a type specimen book. It has survived not
                    only five centuries, but also the leap
                </p>
            </div>
            <div class="poster">
                <img class="poster__img blur"
                     data-lazy="<?= get_template_directory_uri() . '/public/assets/img/banner-bg.png'; ?>"
                     src="<?= get_template_directory_uri() . '/public/assets/img/banner-bg-min.png'; ?>"
                     alt="photo-01"/>
            </div>
        </div>
    </div>
</section>