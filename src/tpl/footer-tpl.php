<div class="footer-box">
    <div class="footer-box__info">
        Lorem ipsum dolor sit amet, consectetur adipisicing elit,<br>
        sed do eiusmod tempor incididunt ut labore et dolore magna
        aliqua. Ut enim ad minim veniam, quis nostrud exercitation
        ullamco laboris nisi ut aliquip ex ea commodo consequat.
    </div>
    <?php
    wp_nav_menu(array(
        'menu_class' => 'navigation-list',
        'menu_id' => 'menu-list-bottom',
        'theme_location' => 'bottom',
        'container' => 'div',
        'container_class' => 'navigation',
        'container_id' => 'nav-bottom',
        'after' => ''
    ));
    ?><!-- #navigation -->
    <img class="footer-box__img"
         src="<?= get_template_directory_uri() . '/public/assets/img/logo-DCMA.png'; ?>"
         alt="DCMA">
    <a href="#" class="btn btn-secondary visit-site">Visit Site</a>
</div>