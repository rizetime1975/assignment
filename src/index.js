const imgPath = localizePath["img"];
import './assets/scss/index.scss';
import $ from "jquery";
import {usersCounterSaved, usersCounter} from './js/users-counter.js';
import {addCtaBg} from './js/add-cta-bg';
import {simpleLazyLoad} from './js/simple-lazy-load';
import {sliderProgress} from './js/slider-progress';
require('slick-carousel');


$(document).ready(function($){
    usersCounterSaved($);
    addCtaBg($, imgPath);
    simpleLazyLoad($);
    sliderProgress($)

    $('.slider-box-road').slick({
        centerPadding: '60px',
        slidesToShow: 3,
        autoplay: true,
        autoplaySpeed: 3000,
        speed: 500,
        prevArrow: $('.custom-arrows .prev'),
        nextArrow: $('.custom-arrows .next')
    });

    const slideCounters = document.querySelectorAll('.slider-item-info .counter .dynamic-count');
    slideCounters.forEach(
        (slideCounter)=>{
            usersCounter($, slideCounter);
        }
    );

});



