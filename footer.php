<?php
/**
 * The template for displaying the footer
 */

?>

</main><!-- #content -->

<footer id="footer-page" class="footer">
    <div class="container">
        <?php get_template_part('public/tpl/footer-tpl');?>
    </div>
</footer><!-- #footer-page -->

</div><!-- #page -->

<?php wp_footer(); ?>

</body>
</html>
