<?php
    $path_to_img = get_template_directory_uri() . '/public/assets/img'; ?>

<section id="top-services" class="top-services">
    <div class="container">
        <div id="slider" class="slider">
            <div class="slider-panel">
                <div class="slider-panel__title">Top Services</div>
                <div class="custom-arrows">
                    <div class="arrow next">
                        <img src="<?= $path_to_img . '/arrows/arrow-slide.png'; ?>"
                             alt="slider-arrow"/>
                    </div>
                    <div class="arrow prev">
                        <img src="<?= $path_to_img . '/arrows/arrow-slide.png'; ?>"
                             alt="slider-arrow"/>
                    </div>
                </div>
                <div class="progress" role="progressbar" aria-valuemin="0" aria-valuemax="100">
                    <span class="slider__label sr-only"> </span>
                </div>
            </div>
            <div class="slider-box">
                <div class="slider-box-road">
                    <?php $placeholder_data = json_decode(file_get_contents("https://jsonplaceholder.typicode.com/comments"));
                    for ($i = 0; $i < 5; $i++) {
                        $id         = $placeholder_data[$i] -> id;
                        $name       = $placeholder_data[$i] -> name;
                        $email      = $placeholder_data[$i] -> email;
                        $comment    = $placeholder_data[$i] -> body;
                        ?>
                        <div class="slider-item" data-id="<?= $id; ?>">
                            <div class="slider-item-photo">
                                <img src="<?= $path_to_img . '/photo-slide.png' ?>"
                                     alt="photo-slide"/>
                            </div>
                            <div class="slider-item-info">
                                <div class="title">Lorem Ipsum</div>
                                <div class="counter">
                                    <div class="counter-item dynamic-count"><?= rand(1, 35) + 2027;?></div>
                                    <div class="counter-item">93%</div>
                                    <div class="counter-item">
                                        <img src="<?= $path_to_img . '/icons/star.png'; ?>"
                                             alt="Star"/>
                                        <span>9.3</span>
                                    </div>
                                </div>
                                <div class="links">
                                    <a href="#" class="read-review">
                                        <span>Read Review</span>
                                        <img src="<?= $path_to_img . '/arrows/right-arrow.png'; ?>"
                                             alt="arrow">
                                    </a>
                                    <a href="#" class="btn btn-primary visit-site">Visit Site</a>
                                </div>
                                <div class="feedback">
                                    <div class="feedback-name">
                                        <img src="<?= $path_to_img . '/icons/communications.png'; ?>"
                                             alt="communications"/>
                                        <span><?= $name; ?></span>
                                    </div>
                                    <div class="feedback-comment"><?= $comment; ?></div>
                                    <div class="feedback-email"><?= $email; ?></div>
                                </div>
                            </div>

                        </div>
                    <?php }?>
                </div>
            </div>
        </div>
    </div>
</section>

