<div class="navigation-wrapper">
    <?php
    wp_nav_menu(array(
        'menu_class' => 'navigation-list',
        'menu_id' => 'menu-list-top',
        'theme_location' => 'top',
        'container' => 'nav',
        'container_class' => 'navigation',
        'container_id' => 'nav-top',
        'after' => ''
    ));
    ?><!-- #navigation -->
    <a href="#" class="btn btn-primary visit-site">Visit site</a>
</div>

