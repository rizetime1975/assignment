<?php
/**
 * The header for our theme
 */
?><!doctype html>
<html <?php language_attributes(); ?>>
<head>
    <meta charset="<?php bloginfo( 'charset' ); ?>" />
    <meta name="viewport" content="width=device-width, initial-scale=1" />
    <link href="https://fonts.googleapis.com/css2?family=Roboto:ital,wght@0,300;0,400;0,700;1,300;1,400;1,700&display=swap" rel="stylesheet">
    <?php wp_head(); ?>
</head>

<body>
<div id="page" class="site">
    <header id="header-page" class="header">
        <div class="header-box">
            <div class="container header-box__container">
                <?php get_template_part('public/tpl/logo');?>
                <?php get_template_part('public/tpl/menu');?>
            </div>
        </div>
    </header><!-- #header-page -->
    <main id="content" class="site-content">
